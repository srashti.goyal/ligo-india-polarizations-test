#!/home1/srashti.goyal/py36_srashti/bin/python
import argparse
__author__ = 'srashti.goyal'

import bilby
import numpy as np
import matplotlib.pylab as plt
import sys, string  
import os

parser = argparse.ArgumentParser(description='This is stand alone code for running lensed injections PE using bilby ')
parser.add_argument('-odir','--odir', help='Output directory',required=True)
parser.add_argument('-inj_id','--inj_id', type=int, help='Injection ID',required=True)
parser.add_argument('-img_no','--img_no', type=int, help='Image number (1 or 2)',required=True)
parser.add_argument('-inj_file','--inj_file', help='Injection parameter .npz file',required=True)
parser.add_argument('-inj_model','--inj_model', help='Injection hypothesis polarization model',default='T')
parser.add_argument('-hyp_model','--hyp_model', help='Recovery hypothesis polarization model',default='T')

args = parser.parse_args()
#injection id
index=args.inj_id
# injection model
pol_inj=args.inj_model

# recovery model
pol_recovery=args.hyp_model

#image no.
img=args.img_no

# load lensed events dataset
inj_data=np.load(args.inj_file)

#output directory
outdir=args.odir
pol_dict={'T':0,'V':1,'S':2}

# injection parameters
m1=inj_data['m1'][index].astype('float64')
m2=inj_data['m2'][index].astype('float64')
dL=inj_data['dist'][index][img-1].astype('float64')
t0=inj_data['t0'][index][img-1].astype('float64')
phi0=inj_data['phi0'][index].astype('float64')
psi=inj_data['pol'][index].astype('float64')
iota=inj_data['incl'][index].astype('float64')
ra=inj_data['ra'][index].astype('float64')
dec=inj_data['dec'][index].astype('float64')

# including injection hypothesis as a flag 'pol'
injection_parameters = dict(
    mass_1=m1, mass_2=m2, luminosity_distance=dL,  psi=psi,theta_jn = iota,
    phase=phi0, geocent_time=t0, ra=ra, dec=dec,pol=pol_dict[pol_inj])

label = 'img'+str(img) 

# load PSD files
psd_files={'H1':'ligo_psd_pycbc.dat','L1':'ligo_psd_pycbc.dat','V1':'virgo_psd_pycbc.dat'}

duration = 4.
sampling_frequency = 2048.
outdir = outdir+'/'+str(index)+'/'+ pol_inj+ '-' + pol_recovery
bilby.core.utils.setup_logger(outdir=outdir, label=label)

waveform_arguments = dict(waveform_approximant='IMRPhenomPv2',
                          reference_frequency=20., minimum_frequency=18.)

# Define polarization models waveforms depending on hypothesis flag 'pol'
def pol_model(frequency_array,mass_1, mass_2, luminosity_distance,theta_jn,phase,pol=0):
    
    waveform_polarizations = bilby.gw.source.lal_binary_black_hole(frequency_array,mass_1=mass_1,mass_2=mass_2,luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,a_1=0, a_2=0, tilt_1=0, tilt_2=0, phi_12=0,phi_jl=0,waveform_arguments =waveform_arguments)
    if pol == 1 :  
        waveform_polarizations['x'] =  waveform_polarizations['plus']
        waveform_polarizations['y'] =  waveform_polarizations['cross']
        del waveform_polarizations['cross']
        del waveform_polarizations['plus']
    if pol == 2 :  
        waveform_polarizations['breathing'] =  waveform_polarizations['plus']
        waveform_polarizations['longitudinal'] =  waveform_polarizations['cross']
        del waveform_polarizations['cross']
        del waveform_polarizations['plus']
    return waveform_polarizations

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=pol_model)

# GW detectors
ifos1 = bilby.gw.detector.InterferometerList(['H1', 'L1','V1'])

# inject zero noise signal
ifos1.set_strain_data_from_zero_noise(sampling_frequency=sampling_frequency, duration=duration,
   start_time=injection_parameters['geocent_time'] - 3)

injection = ifos1.inject_signal(waveform_generator=waveform_generator,parameters=injection_parameters)

# add PSDs to detectors from files loaded
for ifo in ifos1:
        ifo.power_spectral_density =\
                bilby.gw.detector.PowerSpectralDensity(psd_file=psd_files[ifo.name])
        
# Define priors for BBH parameters and for recovery hypothesis model using flag 'pol'
priors = bilby.gw.prior.BBHPriorDict()

del priors['chirp_mass']
del priors['mass_ratio']

priors['geocent_time'] = bilby.core.prior.Uniform(
    minimum=injection_parameters['geocent_time'] - 1,
    maximum=injection_parameters['geocent_time'] + 1,
    name='geocent_time', latex_label='$t_c$', unit='$s$')

if (injection_parameters['mass_1'] < 20) & (injection_parameters['mass_2']< 20):
    m_min = 3.
    m_max=80.
elif (injection_parameters['mass_1'] <50) & (injection_parameters['mass_2']<50):
    m_min = 8.
    m_max=100.
elif (injection_parameters['mass_1']<100) & (injection_parameters['mass_2']<100):
    m_min = 10.
    m_max=200.
else:
    m_min = 50.
    m_max=500.

if injection_parameters['luminosity_distance']<1000:
    dl_min=10.
    dl_max=5000.

elif injection_parameters['luminosity_distance']<5000:
    dl_min=100.
    dl_max=1e4

elif injection_parameters['luminosity_distance']<10000.:
    dl_min=200.
    dl_max=2e4
else:
    dl_min=100.
    dl_max=injection_parameters['luminosity_distance']+1e4
                
priors['mass_1'] = bilby.core.prior.Uniform(m_min,m_max,name='mass_1', latex_label='$m_1$', unit='$Msol$')
priors['mass_2'] = bilby.core.prior.Uniform(m_min,m_max,name='mass_2', latex_label='$m_2$', unit='$Msol$')
priors['luminosity_distance'] =  bilby.gw.prior.UniformComovingVolume(name='luminosity_distance', minimum=dl_min, maximum=dl_max, unit='Mpc')

priors.pop('a_1')
priors.pop('a_2')
priors.pop('tilt_1')
priors.pop('tilt_2')
priors.pop('phi_jl')
priors.pop('phi_12')

priors['pol']=pol_dict[pol_recovery]

# inbuilt gaussian likelihood
likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=ifos1, waveform_generator=waveform_generator)

# run sampler setting up npool
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, sampler='dynesty', nlive=1000,
    injection_parameters=injection_parameters, outdir=outdir, label=label,dlogz=0.01,npool=2)

result.plot_corner(save=True,tiltes=1)