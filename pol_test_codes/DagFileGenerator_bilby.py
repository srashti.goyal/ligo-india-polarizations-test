#!/home1/srashti.goyal/py36_srashti/bin/python

import argparse
__author__ = 'srashti.goyal'

import sys,os
import numpy as np
import math
 
parser = argparse.ArgumentParser(description='This is stand alone code for generating Condor file for parellalizing lensed injection pe runs')
parser.add_argument('-dagfile','--dagfile', help='Name of Dag file',default='dag_pe_run')
parser.add_argument('-inj_file','--inj_file', help='NPZ file with injection parameters',required=True)
parser.add_argument('-odir','--odir', help='Output head directory',required=True)
parser.add_argument('-idx0','--idx0', type=int, help='Starting index',default=0)
parser.add_argument('-n','--n', type=int, help='Number of injections',default=10)
parser.add_argument('-model','--model', help='inj_model-hyp_model; eg:T-T',default='T-T')
args = parser.parse_args()

dagfile = args.dagfile
inj_file = args.inj_file
odir = args.odir
idx0 = args.idx0
n = args.n
model = args.model
inj_model = model.split('-')[0]
hyp_model = model.split('-')[1]
idx=np.arange(537)
os.system('mkdir -p %s'%odir)
os.system('mkdir -p %s/logs'%odir)

os.system('rm %s_pure_pol_reruns_%s.dag'%(dagfile,model)) 
text_file = open('%s_pure_pol_reruns_%s.dag'%(dagfile,model), "w")

for nn,index in enumerate(idx[idx0:idx0+n]):
    for mm,img in enumerate(['img1','img2']):
            run_dir = '%s/%d/%s/%s'%(odir,index,model,img)
            if os.path.isfile(run_dir+'_result.json')== False:
                os.system('rm %s*'%(run_dir)) 
                text_file.write("JOB job%d_%s sub_pe_run.sub\n"%(index,img))
                text_file.write('VARS job%d_%s '%(index,img))
                text_file.write('odir="%s" inj_file = "%s" inj_model="%s" hyp_model="%s" inj_id="%d" img_no= "%d" log_tag="%s/logs/%d_%s_%s"'%(odir,inj_file,inj_model,hyp_model,index,mm+1,odir,index,img,model))
                text_file.write('\n\n')
                
text_file.close()
